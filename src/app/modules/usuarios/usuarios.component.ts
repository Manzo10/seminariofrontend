import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Usuario } from 'src/app/core/models/usuario';
import { UsuariosService } from 'src/app/core/services/usuarios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuario = new Usuario();
  public rols: any;
  public usuarios: any;
  title = 'sweetalert';

  //Se inicializan los botones inhabilitados
  isBtnGuardar: boolean = true;
  isBtnEditar: boolean = true;
  isBtnEliminar: boolean = true;

  //Se inicializan los inputs inhabilitados
  isInputId: boolean = true;
  isInputNombre: boolean = true;
  isInputRol: boolean = true;
  isInputIdActivo: boolean = true;

  constructor(
    private _service: UsuariosService,
  ) { }

  ngOnInit() {
    this.consultarRols();
  }

  //funcion que permite guardar el usuario.
  guardarUsuario() {

    this._service.usuariosFromRemote(this.usuario).subscribe(
      data => {
        this.limpiarCampos();
        Swal.fire(
          'Muy bien!',
          'Se agrego correctamente a' + ' ' + data.nombre,
          'success'
        )
      },
      error => console.log("Revienta")
    );

  }

  consultarRols() {

    this._service.obtenerRols().subscribe(
      data => this.rols = data,
      error => console.log("Revienta")
    );

  }

  listarUsuarios(nombre) {

    this._service.obtenerUsuarioNombre(nombre).subscribe(
      data => {
        console.log(data);
        this.usuarios = data
      },
      error => console.log("Revienta")
    );
  }

  limpiar() {

    this.limpiarCampos();
    this.isBtnGuardar = false;
    this.isBtnEliminar = true;
    this.isBtnEditar = true;
    this.isInputNombre = false;
    this.isInputRol = false;
    this.isInputIdActivo = false;
  }

  editar(user) {

    this.isBtnGuardar = true;
    this.isBtnEditar = false;
    this.isBtnEliminar = false;
    this.isInputNombre = false;
    this.isInputRol = false;
    this.isInputIdActivo = false;

    this.usuario = user;
  }

  crear() {
    this.limpiarCampos();
    this.isBtnGuardar = false;
    this.isBtnEliminar = true;
    this.isBtnEditar = true;
    this.isInputNombre = false;
    this.isInputRol = false;
    this.isInputIdActivo = false;
  }

  //Funcion que permite editar el usuario.
  editarUsuario() {
    let nombre = this.usuario.nombre;
    this._service.editarUsuario(this.usuario).subscribe(
      data => {
        Swal.fire(
          'Muy bien!',
          'Se modifico correctamente!',
          'success'
        )
        this.listarUsuarios(nombre);
      },
      error => console.log("Revienta")
    );
  }

  //Funcion que permite eliminar por medio de Id
  eliminarUsuario(usuario) {

    this._service.eliminarUsuario(usuario.id).subscribe(
      data => {
        this.limpiarCampos();
        Swal.fire(
          'Muy bien!',
          'Se acaba de eliminar correctamente!',
          'success'
        )
        this.listarUsuarios(usuario.nombre);
      },
      error => console.log("Revienta")
    );
  }

  //Funcion que pone en null los campos del formulario
  limpiarCampos(){
    this.usuario = { 'id': null, 'rol': null, 'nombre': '', 'activo': '' };
  }
}
