import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';
import{ HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  urlUsuario:string = "/api/usuario/";
  urlVehiculo:string = "/api/tipoVehiculo/";
  urlRol:string = "/api/rol/";

  constructor( private http: HttpClient) { }

  public usuariosFromRemote( usuario : Usuario):Observable<any>{
    let direccion = this.urlUsuario + "crear";
    return this.http.post<any>(direccion, usuario);
  }

  public obtenerUsuarioNombre(nombre:String){
    let direccion = this.urlUsuario + "nombre?nombre="+nombre;
    return this.http.get<any>(direccion);
  }

  public editarUsuario(usuario : Usuario):Observable<any>{
    console.log(usuario);
    let direccion = this.urlUsuario + "editar?id_usuario="+usuario.id;
    return this.http.put<any>(direccion, usuario);
  }

  public eliminarUsuario(idUsuario){
    let direccion = this.urlUsuario + "eliminar?id_usuario="+idUsuario;
    return this.http.get<any>(direccion);
  }

  public obtenerRols(){
    let direccion = this.urlVehiculo + "todo";
    return this.http.get<any>(direccion);
  }  
  
}
